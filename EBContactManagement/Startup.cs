﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EBContactManagement.Startup))]
namespace EBContactManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
