﻿using System;
using System.Collections.Generic;
using System.Linq;
using EBContactManagement.Controllers;
using EBContactManagement.Models.ViewModel;
using log4net;

namespace EBContactManagement.Models.Service
{
    public class HouseholdService
    {
        private EBContactEntities _db;
        protected readonly ILog _logger = LogManager.GetLogger(typeof(HouseholdService));

        public HouseholdService()
        {
            _db = new EBContactEntities();
            _db.Database.Log = (dbLog => _logger.Info(dbLog));
        }

        public List<Status> GetStatusList()
        {
            return _db.Status.ToList();
        }

        public List<HouseholdListView> getHouseHolds()
        {
            var list = _db.HouseHolds.OrderByDescending(h=>h.ID)
                .Where(h=>h.Deleted == false)
                .Select(h => new HouseholdListView
                {
                    ID = h.ID,
                    Status = h.Status1.Status1,
                    PeopleNames = h.People.Select(p => p.LastName + ", " + p.FirstName),
                    LastContact = h.ContactMades.OrderByDescending(x=>x.ContactDate).Select(x=>x.ContactDate).FirstOrDefault()
                }).ToList();

            list = list.Select(h => new HouseholdListView
            {
                ID = h.ID,
                Status = h.Status,
                Name = h.PeopleNames.Aggregate(string.Empty,
                    (x, y) => x == string.Empty ? y : x + "/" + y),
                LastContact = h.LastContact,
            }).ToList();

            return list;
        }


        public class HouseholdList
        {
            public List<HouseholdListViewBrief> List { get; set; }
            public int Total { get; set; }

        }

        public HouseholdList getHouseHolds(int? current, int? rowCount, List<HouseHoldsController.SortEntry> sort,  List<int> statusList, string startDate, string endDate,string searchPhrase = null)
        {
            var result = new HouseholdList();

            var query = _db.HouseHolds
                .Where(h => h.Deleted == false)
                .Select(h => new HouseholdListView
                {
                    ID = h.ID,
                    Status = h.Status1.Status1,
                    PeopleNames = h.People.Select(p => p.LastName + ", " + p.FirstName),
                    LastContact = h.ContactMades.OrderByDescending(x => x.ContactDate).Select(x => x.ContactDate)
                        .FirstOrDefault()
                }).ToList();

            var list = query.Select(h => new HouseholdListViewBrief
            {
                ID = h.ID,
                Status = h.Status,
                Name = h.PeopleNames.Aggregate(string.Empty,
                    (x, y) => x == string.Empty ? y : x + "/" + y),
                LastContact = h.LastContact,
            }).ToList();

            
            if (!string.IsNullOrEmpty(searchPhrase))
            {
                list = list
                    .Where(x => x.Name.Contains(searchPhrase) 
                                || ( x.Status != null && x.Status.Contains(searchPhrase) )
                                || x.ID.ToString().Contains(searchPhrase) 
                                || ( x.LastContact.HasValue &&  x.LastContact.Value.ToString("d").Contains(searchPhrase))
                                ).ToList();
            }


            if (statusList != null && statusList.Any(x=>x.ToString() != "0") )
            {
                var statusListPruned = statusList.Where(x => x.ToString() != "0").ToList();
                var possibleStatuses = GetStatusList();
                var statusNameList = new List<string>();
                foreach (var status in statusListPruned)
                {
                    var whatever = possibleStatuses.Where(x => x.ID == status).Select(y => y.Status1).Single();
                    statusNameList.Add(whatever);
                }

                list = list.Where(x => statusNameList.Contains(x.Status)).ToList();
            }

            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime myDate;
                if (DateTime.TryParse(startDate, out myDate))
                {
                    list = list.Where(x => x.LastContact.HasValue && x.LastContact.Value >= myDate).ToList();
                }
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime myDate;
                if (DateTime.TryParse(endDate, out myDate))
                {
                    list = list.Where(x => x.LastContact.HasValue && x.LastContact.Value <= myDate).ToList();
                }
            }

            result.Total = list.Count;



            list.Sort(new CompareItems(sort));

            var skipCount = (current.Value - 1) * rowCount.Value;

            result.List = list.Skip(skipCount).Take(rowCount.Value).ToList();

            return result;
        }

        private  class CompareItems:   IComparer<HouseholdListViewBrief> 
        {
            private readonly List<HouseHoldsController.SortEntry> _sort;

            public CompareItems(List<HouseHoldsController.SortEntry> sort)
            {
                _sort = sort;
            }
            public int Compare(HouseholdListViewBrief x, HouseholdListViewBrief y)
            {
                for (int i = 0; i < _sort.Count; i++)
                {
                    var c = CompareField(_sort[i], x, y);
                    if (c != 0)
                    {
                        return c;
                    }

                }

                return 0;
            }

            private int CompareField(HouseHoldsController.SortEntry key, HouseholdListViewBrief x,
                HouseholdListViewBrief y)
            {
                var retVal = 0;
                switch (key.Key)
                {
                    case "Name":
                        retVal = string.CompareOrdinal(x.Name, y.Name);
                        break;
                    case "ID":
                        retVal = x.ID.CompareTo(y.ID);
                        break;
                    case "Status":
                        retVal = string.CompareOrdinal(x.Status, y.Status);
                        break;
                    case "LastContact":
                        if (x.LastContact.HasValue)
                        {
                            retVal = x.LastContact.Value.CompareTo(y.LastContact);
                        }
                        else
                        {
                            retVal = y.LastContact.HasValue ? -1 : 0;
                        }
                        break;

                    default:
                        throw new NotImplementedException();

                }

                if (key.Direction != "asc")
                {
                    retVal = -1 * retVal;
                }
                return retVal;
            }
        }

        public void removeHouseHold(int id)
        {
            var houseHold = _db.HouseHolds.Find(id);
            removeHousehold(houseHold);
            _db.SaveChanges();
        }

        private void removeHousehold(HouseHold houseHold)
        {
            houseHold.Deleted = true;
            if (noPeople(houseHold) && houseHold.ContactMades.Count == 0 &&
                houseHold.BusinessMeetingAttendeds.Count == 0 && houseHold.WorkDayAttendeds.Count == 0 && string.IsNullOrEmpty(houseHold.Notes))
            {
                deleteHousehold(houseHold);
            }
        }

        private static bool noPeople(HouseHold houseHold)
        {
            if (houseHold.People.Count == 0)
            {
                return true;
            }

            if (houseHold.People.Count == 1)
            {
                var person = houseHold.People.Single();
                if (person.PersonPhones.Count == 0 && string.IsNullOrEmpty(person.Email) &&
                    string.IsNullOrEmpty(person.LastName) && string.IsNullOrEmpty(person.LastName)
                    && string.IsNullOrEmpty(person.Street) && string.IsNullOrEmpty(person.City) && string.IsNullOrEmpty(person.State) && string.IsNullOrEmpty(person.Zip)
                    )
                {
                    return true;
                }

            }

            return false;
        }


        private void deleteHousehold(HouseHold houseHold)
        {
            foreach (var person in houseHold.People)
            {
                _db.PersonPhones.RemoveRange(person.PersonPhones);
            }

            _db.People.RemoveRange(houseHold.People);
            _db.ContactMades.RemoveRange(houseHold.ContactMades);
            _db.BusinessMeetingAttendeds.RemoveRange(houseHold.BusinessMeetingAttendeds);
            _db.WorkDayAttendeds.RemoveRange(houseHold.WorkDayAttendeds);
            _db.HouseHolds.Remove(houseHold);
        }

        public void updateModel(HouseHold houseHoldFromDb, HouseHold houseHold)
        {
            houseHoldFromDb.Status = houseHold.Status;
            houseHoldFromDb.Priority = houseHold.Priority;
            houseHoldFromDb.ProspectDate = houseHold.ProspectDate;
            houseHoldFromDb.InitialContact = houseHold.InitialContact;
            houseHoldFromDb.AssociateDate = houseHold.AssociateDate;            
            houseHoldFromDb.AssociateGuide = houseHold.AssociateGuide;            
            houseHoldFromDb.ForumDate = houseHold.ForumDate;            
            houseHoldFromDb.MetWith = houseHold.MetWith;            
            houseHoldFromDb.MemberDate = houseHold.MemberDate;            
            houseHoldFromDb.MoveInDate = houseHold.MoveInDate;            
            houseHoldFromDb.LastContact = houseHold.LastContact;            
            houseHoldFromDb.NextContact = houseHold.NextContact;            
            houseHoldFromDb.Notes = houseHold.Notes;
            houseHoldFromDb.HowHeard = houseHold.HowHeard;

            foreach (var person in houseHold.People)
            {
                var personFromDb = houseHoldFromDb.People.Single(m => m.ID == person.ID);
                personFromDb.FirstName = person.FirstName;
                personFromDb.LastName = person.LastName;
                personFromDb.Street = person.Street;
                personFromDb.City = person.City;
                personFromDb.State = person.State;
                personFromDb.Zip = person.Zip;
                personFromDb.Email = person.Email;
                foreach (var phone in person.PersonPhones)
                {
                    var phoneFromDb = personFromDb.PersonPhones.Single(m => m.ID == phone.ID);
                    if (!string.IsNullOrEmpty(phone.PhoneNumber))
                    {
                        phoneFromDb.PhoneNumber =
                            phone.PhoneNumber.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
                    }
                    else
                    {
                        phoneFromDb.PhoneNumber = phone.PhoneNumber;
                    }
                    phoneFromDb.Type = phone.Type;
                    phoneFromDb.Primary = phone.Primary;                    
                }             
            }
            foreach (var contact in houseHold.ContactMades)
            {
                var contactFromDb = houseHoldFromDb.ContactMades.Single(m => m.ID == contact.ID);
                contactFromDb.ContactDate = contact.ContactDate;
                contactFromDb.ContactTypeID = contact.ContactTypeID;
                contactFromDb.ContactedByID = contact.ContactedByID;
                contactFromDb.Notes = contact.Notes;
            }
            foreach (var meeting in houseHold.BusinessMeetingAttendeds)
            {
                var meetingFromDb = houseHoldFromDb.BusinessMeetingAttendeds.Single(m => m.ID == meeting.ID);
                meetingFromDb.AttendedDate = meeting.AttendedDate;
            }
            foreach (var workday in houseHold.WorkDayAttendeds)
            {
                var workdayFromDb = houseHoldFromDb.WorkDayAttendeds.Single(m => m.ID == workday.ID);
                workdayFromDb.AttendedDate = workday.AttendedDate;
            }
        }

    }
}