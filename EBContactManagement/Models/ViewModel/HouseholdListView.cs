﻿using System;
using System.Collections.Generic;

namespace EBContactManagement.Models.ViewModel
{
    public class HouseholdListView
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public IEnumerable<string> PeopleNames { get; set; }
        public string Status { get; set; }
        public DateTime? LastContact { get; set; }
    }

    public class HouseholdListViewBrief
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public DateTime? LastContact { get; set; }
    }

}