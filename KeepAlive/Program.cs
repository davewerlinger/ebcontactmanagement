﻿using System;
using System.IO;
using System.Net;

namespace KeepAlive
{
    class Program
    {
        static void Main(string[] args)
        {
            WebRequest request = WebRequest.Create("http://contact.elderberrycohousing.net/HouseHolds/KeepAlive/2");
            WebResponse response = request.GetResponse();
            Stream data = response.GetResponseStream();
            string html = String.Empty;
            using (StreamReader sr = new StreamReader(data))
            {
                html = sr.ReadToEnd();
            }
        }
    }
}
