﻿using System.IO;
using System.Web;
using System.Web.Hosting;
using log4net.Config;

[assembly: PreApplicationStartMethod(typeof(EBContactManagement.App_Start.log4net), "Start")]

namespace EBContactManagement.App_Start
{
    public class log4net
    {
        public static void Start()
        {
            var path = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "log4net.config");
            var fi = new FileInfo(path);
            XmlConfigurator.ConfigureAndWatch(fi);
        }
    }
}



