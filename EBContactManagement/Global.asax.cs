﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using EBContactManagement.Models;
using Hangfire;
using InteractivePreGeneratedViews;
using log4net;
using GlobalConfiguration = Hangfire.GlobalConfiguration;

namespace EBContactManagement
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static int sessionCount;
        private static object countLock;
        public static ILog log = null;
        private BackgroundJobServer _backgroundJobServer = null;

        protected void Application_Start()
        {
            sessionCount = 0;
            countLock = new object();
            log = LogManager.GetLogger("Global.asax");
            log.Info("Starting app");

            System.Web.Http.GlobalConfiguration.Configure(WebApiConfig.Register);
            //AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            log.Debug("finished app start");

            configHangFire();

            using (var ctx = new EBContactEntities())
            {
                InteractiveViews
                    .SetViewCacheFactory(
                        ctx,
                        new SqlServerViewCacheFactory(ctx.Database.Connection.ConnectionString));
            }

            //timeFirstContextAccess();
        }

        private static void timeFirstContextAccess()
        {
            Stopwatch st = new Stopwatch();
            st.Start();
            var x = new EBContactEntities().HouseHolds.FirstOrDefault();
            st.Stop();
            log.Debug("First time " + st.ElapsedMilliseconds);
            st.Reset();
            st.Start();
            x = new EBContactEntities().HouseHolds.FirstOrDefault();
            st.Stop();
            log.Debug("Second time " + st.ElapsedMilliseconds);
        }

        private void configHangFire()
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage("EBContact");
            _backgroundJobServer = new BackgroundJobServer();
            RecurringJob.AddOrUpdate(() => keepAlive(), "*/10 5-23 * * *", TimeZoneInfo.FindSystemTimeZoneById("US Eastern Standard Time"));
        }

        public void keepAlive()
        {
            log.Info("Keeping alive");
            try
            {
                WebRequest request = WebRequest.Create("http://contact.elderberrycohousing.com/HouseHolds/KeepAlive/2");
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string html = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    html = sr.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                log.Error("Hangfire ", ex);
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = Server.GetLastError();
            if (ex != null)
            {
                Trace.TraceError(ex.ToString());
                var stuff = "Global error - ";
                try
                {
                    stuff += User.Identity.IsAuthenticated ? User.Identity.Name : Session.SessionID;
                    stuff += " [" + Request.UserAgent + "]";
                }
                catch (Exception)
                {
                }
                stuff += ": ";
                log.Error(stuff, ex);
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            int saveCount;
            lock (countLock)
            {
                sessionCount += 1;
                saveCount = sessionCount;
            }
            log.Info(string.Format("{1} - session started {0}.", saveCount, Session.SessionID));

        }

        protected void Session_End(object sender, EventArgs e)
        {
            int saveCount;
            lock (countLock)
            {
                sessionCount -= 1;
                saveCount = sessionCount;
            }
            log.Info(string.Format("{1} - session ended {0}.", saveCount, Session.SessionID));
            var zone = TimeZoneInfo.FindSystemTimeZoneById("US Eastern Standard Time");
            var currentTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, zone);
            var fiveAm = DateTime.Today.AddHours(5);
            if (saveCount == 0 && currentTime > fiveAm)
            {
                //keepAlive();
            }

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            log.Debug("Begin request");
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            log.Debug("End request");
        }

        protected void Application_End(object sender, EventArgs e)
        {
            log.Info("Ending application");
            _backgroundJobServer?.Dispose();
        }
    }
}
