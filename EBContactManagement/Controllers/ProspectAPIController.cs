﻿using System;
using System.Web.Http;
using EBContactManagement.Models;
using EBContactManagement.Models.Service;
using MiraComp.MVC.FilterAttribute;
using log4net;

namespace EBContactManagement.Controllers
{
    public class Prospect
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Comment { get; set; }
    }

    [LogException]
    public class ProspectAPIController : ApiController
    {
        private HouseholdService _householdService = new HouseholdService();
        private EBContactEntities db = new EBContactEntities();
        private ILog _log = LogManager.GetLogger(typeof(ProspectAPIController));

        [HttpPost]
        public IHttpActionResult AddProspect([FromBody]Prospect p)
        {
            var houseHold = new HouseHold {Status = 1};

            var contactMade = new ContactMade
            {
                ContactTypeID = 4,
                ContactDate = DateTime.Today,
                Notes = p.Comment
            };
            houseHold.ContactMades.Add(contactMade);
            var person = new Person
            {
                FirstName = p.FirstName,
                LastName = p.LastName,
                Street = p.Address1,
                City = p.City,
                State = p.State,
                Zip = p.Zip,
                Email = p.Email
            };
            var phone = new PersonPhone
            {
                PhoneNumber = p.Phone,
                Primary = true
            };
            person.PersonPhones.Add(phone);
            houseHold.People.Add(person);

            db.HouseHolds.Add(houseHold);
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                _log.Error("API error",ex);
            }
            return Ok();
        }
    }
}
