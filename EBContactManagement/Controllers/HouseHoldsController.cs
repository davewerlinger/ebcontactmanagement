﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using CsvHelper;
using EBContactManagement.Models;
using EBContactManagement.Models.Service;
using EBContactManagement.Models.ViewModel;
using log4net;
using MiraComp.MVC;
using MiraComp.MVC.FilterAttribute;
using Newtonsoft.Json;

namespace EBContactManagement.Controllers
{
    [LogException]
    public class HouseHoldsController : Controller
    {
        private EBContactEntities db = new EBContactEntities();
        private HouseholdService _householdService = new HouseholdService();
        protected readonly ILog _logger = LogManager.GetLogger(typeof(HouseHoldsController));

        public HouseHoldsController()
        {
            _logger.Debug("ctor start");
            db.Database.Log = (dbLog => _logger.Info(dbLog));
            _logger.Debug("ctor finish");
        }

        public ActionResult IndexOld()
        {
            _logger.Debug("Index start");
            ViewBag.StatusList = GetStatuslist();
            var list = _householdService.getHouseHolds();
            _logger.Debug("Index finish");
            return View("Index",list);
        }


        public ActionResult Index()
        {
            _logger.Debug("IndexPlus start");
            var userAgent = Request.UserAgent;
            _logger.Info("(admin)User agent: " + userAgent);
            ViewBag.StatusList = GetStatuslist();
            var list = new List<HouseholdListView>();
            _logger.Debug("IndexPlus finish");
            return View("IndexPlus",list);
        }


        private  List<SelectListItem> GetStatuslist()
        {
            var list = _householdService.GetStatusList();

            var selectlist = list.Select(item => new SelectListItem { Text = item.Status1, Value = item.ID.ToString() }).ToList();

            selectlist.Insert(0,new SelectListItem{Text = "--Choose--",Value = "0"});

            return selectlist;
        }

        // GET: HouseHolds/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HouseHold houseHold = db.HouseHolds.Find(id);
            if (houseHold == null)
            {
                return HttpNotFound();
            }
            return View(houseHold);
        }

        // GET: HouseHolds/Create
        public ActionResult Create()
        {
            createSelectLists();
            var houseHold = new HouseHold();
            houseHold.People.Add(new Person());
            db.HouseHolds.Add(houseHold);

            db.SaveChanges();

            return RedirectToAction("Edit", new {id = houseHold.ID});
        }

        // POST: HouseHolds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ID,Status,Priority,ProspectDate,InitialContact,AssociateDate,AssociateGuide,ForumDate,MetWith,MemberDate,MoveInDate,LastContact,NextContact,Notes")] HouseHold houseHold)
        public ActionResult Create( HouseHold houseHold)
        {
            if (ModelState.IsValid)
            {
                db.HouseHolds.Add(houseHold);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            createSelectLists();
            return View(houseHold);
        }

        // GET: HouseHolds/Edit/5
        public ActionResult Edit(int? id)
        {
            _logger.Debug("Edit get start");
            ViewBag.ErrorMessage = string.Empty;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HouseHold houseHold = db.HouseHolds.Find(id);
            if (houseHold == null)
            {
                return HttpNotFound();
            }

            createSelectLists();
            _logger.Debug("Edit get end");
            return View(houseHold);
        }

        private void createSelectLists()
        {
            var contacters = db.Contacters.Where(x => x.Active.HasValue && x.Active.Value).OrderBy(x => x.Priority);
            ViewBag.StatusList = new SelectList(db.Status, "ID", "Status1");
            ViewBag.PhoneType = new SelectList(db.PhoneTypes, "ID", "Description");
            ViewBag.Contacter = new SelectList(contacters, "ID", "Name");
            ViewBag.ContactTypes = new SelectList(db.ContactTypes, "ID", "Name");
        }

        // POST: HouseHolds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ID,Status,Priority,ProspectDate,InitialContact,AssociateDate,AssociateGuide,ForumDate,MetWith,MemberDate,MoveInDate,LastContact,NextContact,Notes")] HouseHold houseHold)
        public ActionResult Edit(HouseHold houseHold)
        {
            ViewBag.ErrorMessage = string.Empty;
            var houseHoldFromDB = db.HouseHolds.Find(houseHold.ID);
            _householdService.updateModel(houseHoldFromDB, houseHold);

            if (ModelState.IsValid)
            {
                db.SaveChanges();
                ViewBag.ErrorMessage = "Changes saved " + DateTime.Now.ToLongTimeString();
            }
            createSelectLists();
            return View(houseHold);
        }

        [HttpPost]
        public ActionResult AddContact(int id)
        {
            var houseHold = db.HouseHolds.Find(id);
            var contact = new ContactMade {ContactDate = DateTime.Today};
            houseHold.ContactMades.Add(contact);
            db.SaveChanges();
           
            createSelectLists();
            var partialView = PartialView("_contact", contact).RenderToString();
            return Json(new { success = true, html = partialView }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemoveContact(int id,int cid)
        {
            var houseHold = db.HouseHolds.Find(id);
            var contact = houseHold.ContactMades.Single(x => x.ID == cid);
            houseHold.ContactMades.Remove(contact);
            db.SaveChanges();

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult AddPerson(int id)
        {
            var houseHold = db.HouseHolds.Find(id);
            var contact = new Person();
            houseHold.People.Add(contact);
            db.SaveChanges();

            createSelectLists();
            var partialView = PartialView("_person", contact).RenderToString();
            return Json(new { success = true, html = partialView }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemovePerson(int id, int pid)
        {
            var houseHold = db.HouseHolds.Find(id);
            var person = houseHold.People.Single(x => x.ID == pid);
            houseHold.People.Remove(person);
            db.SaveChanges();

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult AddBusinessMeeting(int id)
        {
            var houseHold = db.HouseHolds.Find(id);
            var meeting = new BusinessMeetingAttended();
            houseHold.BusinessMeetingAttendeds.Add(meeting);
            db.SaveChanges();

            createSelectLists();
            var partialView = PartialView("_BusinessMeeting", meeting).RenderToString();
            return Json(new { success = true, html = partialView }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemoveBusinessMeeting(int id, int pid)
        {
            var houseHold = db.HouseHolds.Find(id);
            var meeting = houseHold.BusinessMeetingAttendeds.Single(x => x.ID == pid);
            houseHold.BusinessMeetingAttendeds.Remove(meeting);
            db.SaveChanges();

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddWorkday(int id)
        {
            var houseHold = db.HouseHolds.Find(id);
            var workday = new WorkDayAttended();
            houseHold.WorkDayAttendeds.Add(workday);
            db.SaveChanges();

            createSelectLists();
            var partialView = PartialView("_WorkDayAttended", workday).RenderToString();
            return Json(new { success = true, html = partialView }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemoveWorkday(int id, int pid)
        {
            var houseHold = db.HouseHolds.Find(id);
            var workday = houseHold.WorkDayAttendeds.Single(x => x.ID == pid);
            houseHold.WorkDayAttendeds.Remove(workday);
            db.SaveChanges();

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddPhone(int id,int pid, string containerPrefix)
        {
            var houseHold = db.HouseHolds.Find(id);
            var person = houseHold.People.Single(m => m.ID == pid);
            var phone = new PersonPhone();
            person.PersonPhones.Add(phone);
            db.SaveChanges();

            createSelectLists();
            ViewData.TemplateInfo.HtmlFieldPrefix = containerPrefix;
            var partialView = PartialView("_phone", phone).RenderToString();
            return Json(new { success = true, html = partialView }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemovePhone(int id, int pid)
        {
            var houseHold = db.HouseHolds.Find(id);
            PersonPhone phone = null;
            Person person1 = null;
            foreach (Person person in houseHold.People)
            {
                person1 = person;
                phone = person.PersonPhones.SingleOrDefault(p => p.ID == pid);
                if (phone != null) break;
            }
            if (phone != null)
            {
                person1.PersonPhones.Remove(phone);
            }
            db.SaveChanges();

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public class ExportPeopleStuff
        {
            public string Firstname { get; set; }
            public string Lastname { get; set; }
            public string Email { get; set; }
        }



        [HttpPost]
        public ActionResult ExportSelected(List<int> model)
        {
            //https://stackoverflow.com/questions/30704078/how-to-download-a-file-through-ajax-request-in-asp-net-mvc-4

            var exportList = new List<ExportPeopleStuff>();
            var listOfIDs =   db.HouseHolds.Where(h => model.Contains(h.ID));
            foreach (var household in listOfIDs)
            {
                var peoples = household.People.ToList();
                var lastEmail = "";
                for (var i = 0; i < peoples.Count; i++)
                {
                    var person = peoples[i];
                    var x = new ExportPeopleStuff
                        {Firstname = person.FirstName, Lastname = person.LastName, Email = person.Email};
                    if (string.IsNullOrEmpty(person.Email))
                    {
                        // x.Email = lastEmail;
                    }
                    else
                    {
                        lastEmail = person.Email;
                    }
                    exportList.Add((x));
                }
            }

            string fileName = "ExportSelected" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".csv";
            string fileNameWithPath = Path.Combine(Path.GetTempPath(), fileName);

            using (var writer = new StreamWriter(fileNameWithPath))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.WriteRecords(exportList);
            }


            return Json(new { fileName = fileName });
        }

        [HttpGet]
        public ActionResult DownloadSelected(string file)
        {
            var path = Path.Combine(Path.GetTempPath(), file);
            var memory = new MemoryStream();
            try
            {
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    stream.CopyTo(memory);
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("FileNotFoundError", e.Message);
                return Content(e.Message);
            }
            memory.Position = 0;
            return File(memory, "text\\plain", Path.GetFileName(path));
        }

        public class BootgridResponseData<T> where T : class
        {
            public int current { get; set; } // current page
            public int rowCount { get; set; } // rows per page
            public IEnumerable<T> rows { get; set; } // items
            public int total { get; set; } // total rows for whole query
        }

        public class SortEntry
        {
            public string Key { get; set; }
            public string Direction { get; set; }
        }


        [HttpPost]
        public ActionResult Fetch(int? current, int? rowCount, List<int> statusList, string startDate, string endDate, string searchPhrase=null)
        {
            if (!current.HasValue)
            {
                current = 1;
            }

            if (!rowCount.HasValue)
            {
                rowCount = 15;
            }

            var formVariables = Request.Form.AllKeys;
            var sort = new List<SortEntry>();

            for (int i = 0; i < formVariables.Length; i++)
            {
                var key = formVariables[i];
                if (key.StartsWith("sort"))
                {
                    var sortColumn = key.Substring( 5, key.Length - 6);
                    var sortDirection = Request.Params[key];
                    sort.Add(new SortEntry{Key = sortColumn,Direction = sortDirection});
                }
            }

            var householdList = _householdService.getHouseHolds(current, rowCount, sort, statusList,startDate, endDate ,searchPhrase );

            var result = new BootgridResponseData<HouseholdListViewBrief>
            {
                current = current.Value, rowCount = rowCount.Value, rows = householdList.List, total = householdList.Total
            };


            var jsSettings = new JsonSerializerSettings {ReferenceLoopHandling = ReferenceLoopHandling.Ignore};
            var converted = JsonConvert.SerializeObject(result, null, jsSettings);

            return Content(converted, "application/json");
        }


        public ActionResult KeepAlive(int id)
        {
            _logger.Debug("keep alive");
            string whatever;
            db.Database.Log = (dbLog => whatever = dbLog);
            var houseHold = db.HouseHolds.Find(id);
            houseHold.LastContact = null;  // No save to DB
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        // GET: HouseHolds/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HouseHold houseHold = db.HouseHolds.Find(id);
            if (houseHold == null)
            {
                return HttpNotFound();
            }
            return View(houseHold);
        }

        // POST: HouseHolds/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _householdService.removeHouseHold(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
