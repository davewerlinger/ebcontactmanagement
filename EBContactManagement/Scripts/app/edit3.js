﻿var identity;

$(document).ready(function () {
    $("form").areYouSure();

    phoneMask();

    $("form").submit(function () {
        //$(".phone").unmask();
        //$(".phone").val().replace(/()-/g, '');
    });

    identity = $("#ID").val();

    $("textarea").each(function() {
        this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden');
    }).on('input', function() {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
    });

    $("#addcontact").on("click", function (e) {
        e.preventDefault();
        var posting = $.post(addContactUrl,
        {
            id: identity,
            async: false
        });
        posting.done(function (response, status, jqXhr) {
            if (response.success) {
                //JL().info("Add Residence worked");
                $("#tblContacts > tbody").append(response.html);
                $("#tblContacts").removeClass("invisible");
                $("#nocontacts").hide();
                return;
            }
            ajaxDoneError("Add Residence failed " + status + " " + identity, "Add failed", response);
        });
        posting.fail(function (jqXhr, textStatus, errorThrown) {
            ajaxFailed("Add Residence post failed " + textStatus + " " + identity, "error", jqXhr, errorThrown);
        });
        return false;
    });

    $("#tblContacts").on("click",
        function (e) {
            var button = findButton(e.target);
            if (button.name === "DeleteContact") {
                $(button).prop('disabled', true);
                e.preventDefault();
                var posting = $.post(deleteContactUrl,
                {
                    id:identity,
                    cid: button.id
                });
                posting.done(function (response, x, y) {
                    if (response.success) {
                        //JL().info("Delete Residence worked");
                        $(button.parentElement.parentElement).remove();
                    } else {
                        ajaxDoneError("Delete Residence failed" + " " + identity, "Delete failed", response);
                    }
                });
                posting.fail(function (jqXhr, textStatus, errorThrown) {
                    ajaxFailed("Delete Residence post failed" + " " + identity, "error", jqXhr, textStatus, errorThrown);
                });
            }
        });

    $("#addperson").on("click", function (e) {
        e.preventDefault();
        var posting = $.post(addPersonUrl,
        {
            id: identity,
            async: false
        });
        posting.done(function (response, status, jqXhr) {
            if (response.success) {
                //JL().info("Add Residence worked");
                $("#listPeople").append(response.html);
                $("#listPeople").removeClass("invisible");
                $("#nopeople").hide();
                return;
            }
            ajaxDoneError("Add Person failed " + status + " " + identity, "Add failed", response);
        });
        posting.fail(function (jqXhr, textStatus, errorThrown) {
            ajaxFailed("Add Person post failed " + textStatus + " " + identity, "error", jqXhr, errorThrown);
        });
        return false;
    });

    $("#listPeople").on("click",
        function (e) {
            var button = findButton(e.target);
            if (button.name === "DeletePerson") {
                $(button).prop('disabled', true);
                e.preventDefault();
                var posting = $.post(deletePersonUrl,
                {
                    id: identity,
                    pid: button.id
                });
                posting.done(function (response, x, y) {
                    if (response.success) {
                        //JL().info("Delete Residence worked");
                        $(button.parentElement.parentElement).remove();
                    } else {
                        ajaxDoneError("Delete Person failed" + " " + identity, "Delete failed", response);
                    }
                });
                posting.fail(function (jqXhr, textStatus, errorThrown) {
                    ajaxFailed("Delete Person post failed" + " " + identity, "error", jqXhr, textStatus, errorThrown);
                });
            }
        });

    $("button.addphone").on("click", function (e) {
        e.preventDefault();
        var button = findButton(e.target);
        var posting = $.post(addPhoneUrl,
        {
            id: identity,
            pid: button.id,
            containerPrefix: button.getAttribute("data-containerPrefix"),
            async: false
        });
        posting.done(function (response, status, jqXhr) {
            if (response.success) {
                //JL().info("Add Residence worked");
                var tbl = $("#tblPhone_" + button.id);
                tbl.append(response.html);
                phoneMask();
                return;
            }
            ajaxDoneError("Add Phone failed " + status + " " + identity, "Add failed", response);
        });
        posting.fail(function (jqXhr, textStatus, errorThrown) {
            ajaxFailed("Add Phone post failed " + textStatus + " " + identity, "error", jqXhr, errorThrown);
        });
        return false;
    });

    $("table.tblphone").on("click",
        function (e) {
            var button = findButton(e.target);
            if (button.name === "DeletePhone") {
                $(button).prop('disabled', true);
                e.preventDefault();
                var posting = $.post(deletePhoneUrl,
                {
                    id: identity,
                    pid: button.id
                });
                posting.done(function (response, x, y) {
                    if (response.success) {
                        //JL().info("Delete Residence worked");
                        $(button.parentElement.parentElement).remove();
                    } else {
                        ajaxDoneError("Delete Person failed" + " " + identity, "Delete failed", response);
                    }
                });
                posting.fail(function (jqXhr, textStatus, errorThrown) {
                    ajaxFailed("Delete Person post failed" + " " + identity, "error", jqXhr, textStatus, errorThrown);
                });
            }
        });

    $("#addbusinessmeeting").on("click", function (e) {
        e.preventDefault();
        var posting = $.post(addBusinessMeetingUrl,
        {
            id: identity,
            async: false
        });
        posting.done(function (response, status, jqXhr) {
            if (response.success) {
                //JL().info("Add Residence worked");
                $("#businessmeetings").append(response.html);
                return;
            }
            ajaxDoneError("Add Person failed " + status + " " + identity, "Add failed", response);
        });
        posting.fail(function (jqXhr, textStatus, errorThrown) {
            ajaxFailed("Add Person post failed " + textStatus + " " + identity, "error", jqXhr, errorThrown);
        });
        return false;
    });

    $("#businessmeetings").on("click",
        function (e) {
            var button = findButton(e.target);
            if (button.name === "DeleteMeeting") {
                $(button).prop('disabled', true);
                e.preventDefault();
                var posting = $.post(deleteBusinessMeetingUrl,
                {
                    id: identity,
                    pid: button.id
                });
                posting.done(function (response, x, y) {
                    if (response.success) {
                        //JL().info("Delete Residence worked");
                        $(button.parentElement.parentElement).remove();
                    } else {
                        ajaxDoneError("Delete Person failed" + " " + identity, "Delete failed", response);
                    }
                });
                posting.fail(function (jqXhr, textStatus, errorThrown) {
                    ajaxFailed("Delete Person post failed" + " " + identity, "error", jqXhr, textStatus, errorThrown);
                });
            }
        });




    $("#addworkday").on("click", function (e) {
        e.preventDefault();
        var posting = $.post(addWorkdayUrl,
        {
            id: identity,
            async: false
        });
        posting.done(function (response, status, jqXhr) {
            if (response.success) {
                //JL().info("Add Residence worked");
                $("#workdayattendeds").append(response.html);
                return;
            }
            ajaxDoneError("Add Person failed " + status + " " + identity, "Add failed", response);
        });
        posting.fail(function (jqXhr, textStatus, errorThrown) {
            ajaxFailed("Add Person post failed " + textStatus + " " + identity, "error", jqXhr, errorThrown);
        });
        return false;
    });

    $("#workdayattendeds").on("click",
        function (e) {
            var button = findButton(e.target);
            if (button.name === "DeleteWorkday") {
                $(button).prop('disabled', true);
                e.preventDefault();
                var posting = $.post(deleteWorkdayUrl,
                {
                    id: identity,
                    pid: button.id
                });
                posting.done(function (response, x, y) {
                    if (response.success) {
                        //JL().info("Delete Residence worked");
                        $(button.parentElement.parentElement).remove();
                    } else {
                        ajaxDoneError("Delete Person failed" + " " + identity, "Delete failed", response);
                    }
                });
                posting.fail(function (jqXhr, textStatus, errorThrown) {
                    ajaxFailed("Delete Person post failed" + " " + identity, "error", jqXhr, textStatus, errorThrown);
                });
            }
        });

    //setInterval(keepAlive, 30000);
    //function keepAlive() {
    //    var whatever = $.post(keepAliveUrl,
    //        {
    //            id: identity,
    //            async: true
    //        });
    //}


    function findButton(target) {
        if (target.tagName.toLowerCase() === "button") {
            return target;
        }
        return target.parentElement;
    }

    function phoneMask() {
        $(".phone").mask("(999) 999-9999", { autoclear: false });
    }

    function ajaxDoneError(msg, alertmsg, response) {
        //if (response.status === 401) {
        //    alert(request.responseText);
        //    JL().info("Ajax done not success:" + request.responseText);
        //    window.location = logoutUrl;
        //} else {
        //    JL().error(msg);
            alert(alertmsg);
        //}
    }


    function ajaxFailed(msg, alertmsg, jqXhr, textStatus, errorThrown) {
        //if (jqXhr.status === 401) {
        //    alert(jqXhr.responseText);
        //    JL().info("Ajax failed:" + jqXhr.responseText);
        //    window.location = logoutUrl;
        //} else {
        //    JL().error(msg);
            alert(alertmsg);
        //}
    }
});