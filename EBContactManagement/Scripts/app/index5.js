﻿$(document).ready(function () {

    function createGrid(enableMs) {
        $("#tblProspects").bootgrid({
            caseSensitive: false,
            multiSort: enableMs,
            selection: true,
            multiSelect: true,
            formatters:
            {
                "link": function (column, row) {
                    /* "this" is mapped to the grid instance */
                    return "<a target=\"_blank\" href=\"/Households/Edit/" +
                        row.id +
                        "\">Edit</a>" +
                        " | <a href=\"/Households/Delete/" +
                        row.id +
                        "\">Delete</a>";
                }
            },
            rowCount: [15, 25, 50, -1]
        }
        );

    }



    var prospects = $("#tblProspects");
    createGrid(false);


    $("#btnMultisort").click(function (e) {
        $("#tblProspects").bootgrid("destroy");
        createGrid(true);
        $("#btnMultisort").hide();
        e.preventDefault = true;

    });

    $("#btnDown").click(function (e) {
        var rows = $("#tblProspects").bootgrid("getSelectedRows");
        if (rows.length <= 0) {
            $("#spnError").html("Nothing selected");
            e.preventDefault = true;
            return;
        }
        $("#spnError").html("");
        var rowIds = new Array;
        for (var i = 0; i < rows.length; i++) {
            rowIds.push(rows[i]);
        }
        jQuery.ajax({
            type: "POST",
            url: exportUrl,
            dataType: "json",
            data: { model: rowIds },
            success: function (data) {
                window.location.href = downloadUrl + "/?file=" + data.fileName;
            },
            failure:
                function (err, response) {
                    console.log(err, response);
                    alert(err, response.responseText);
                }
        });

        e.preventDefault = true;
    });



    //$("th[data-column-id*='status'] a").before($("#whatever").clone());
    //$("#whatever").remove();
    //$("th[data-column-id*='lastcontact'] a").before("<input type='date'/>");
});